const express = require("express");
const app = express();
const mongoose = require('mongoose');
const path = require("path");

const port = process.env.PORT || 3000;

// Conexión a MongoDB
mongoose.connect(
    `mongodb://root:pass12345@localhost:27017/tutorial?authSource=admin`,
    { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true }
);


mongoose.connection.on('error', console.error.bind(console, 'Error de conexión a MongoDB:'));
mongoose.connection.once('open', () => {
    console.log('Conexión exitosa a MongoDB');
});

// Configuración de Express
app.set("views", path.join(__dirname, "app", "views"));
app.set("view engine", "pug");
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'tu-proyecto-vue/dist')));

// Importar rutas
const routes = require("./app/routes/routes");

// Definir rutas usando URL path
app.use("/", routes);

// Iniciar el servidor
app.listen(port, () => {
    console.log(`Servidor en funcionamiento en el puerto ${port}`);
});
